#!/bin/sh
[ $# -ne '4' ] && echo -e "Usage:\n      bash $0 [WebDAV] [User] [Password] [MountPath]" && exit 1
WebDAV="$1"
User="$2"
Pwd="$3"
WebMount="$4"
yes | sudo DEBIAN_FRONTEND=noninteractive apt-get -yqq install davfs2
[ $? -ne '0' ] && echo "Install davfs2 fail! " && exit 1
mkdir -p "$WebMount"
rm -rf /etc/davfs2/davfs2.conf
cat >>/etc/davfs2/davfs2.conf<<EOF
dav_user        root
dav_group        root
kernel_fs        fuse
buf_size        4
use_locks       0
cache_dir        /tmp/WebDAV
cache_size        4
delay_upload        3
 
EOF
rm -rf /etc/davfs2/secrets
cat >>/etc/davfs2/secrets<<EOF
$WebDAV $User $Pwd
 
EOF
chmod 600 /etc/davfs2/secrets
chmod 644 /etc/davfs2/davfs2.conf
rm -rf /etc/davfs2/webdav.mnt
cat >>/etc/davfs2/webdav.mnt<<EOF
#!/bin/sh
### BEGIN INIT INFO
# Provides:          webdav.mnt
# Required-Start:    \$all
# Required-Stop:     \$network \$remote_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start daemon at boot time
# Description:       Enable davfs by daemon.
### END INIT INFO
DAEMON=/usr/sbin/mount.davfs
PID=/var/run/webdav.pid
WebDAV="$WebDAV"
DavMnt="$WebMount"
 
test -x \$DAEMON || exit 1
case "\$1" in
start)
[ -n "\$(pgrep mount.davfs)" ] && echo "davfs already running. " && exit 0 || {
exec start-stop-daemon --start --pidfile \$PID --exec \$DAEMON \$WebDAV \$DavMnt -- -f >> /dev/null 2>&1 &
sleep 3
[ -n "\$(pgrep mount.davfs)" ] && echo "davfs start success. " || echo "davfs start fail. "
}
;;
stop)
[ -z "\$(pgrep mount.davfs)" ] && echo "davfs not running. " && exit 0 || {
umount \$DavMnt>> /dev/null 2>&1 &
sleep 3
[ -z "\$(pgrep mount.davfs)" ] && echo "davfs stop success. " || echo "davfs stop fail. "
}
;;
restart)
[ -z "\$(pgrep mount.davfs)" ] || {
umount \$DavMnt>> /dev/null 2>&1 &
sleep 3
[ -z "\$(pgrep mount.davfs)" ] && echo "davfs stop success. " || echo "davfs stop fail. "
}
exec start-stop-daemon --start --pidfile \$PID --exec \$DAEMON \$WebDAV \$DavMnt -- -f >> /dev/null 2>&1 &
sleep 3
[ -n "\$(pgrep mount.davfs)" ] && echo "davfs start success. " || echo "davfs start fail. "
;;
*)
echo "Usage: webdav.mnt {start|stop|restart}"
exit 1
esac
exit 0
 
EOF
chmod 755 /etc/davfs2/webdav.mnt
ln -sf /etc/davfs2/webdav.mnt /etc/init.d/webdav.mnt
ln -sf /etc/davfs2/webdav.mnt /usr/local/bin/webdav.mnt
update-rc.d -f webdav.mnt remove >>/dev/null 2>&1
update-rc.d webdav.mnt defaults